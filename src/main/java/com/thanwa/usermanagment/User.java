/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.usermanagment;

import java.io.Serializable;

/**
 *
 * @author tud08
 */
public class User implements Serializable{
    private String userNmae;
    private String password;

    public User(String userNmae, String password) {
        this.userNmae = userNmae;
        this.password = password;
    }

    public String getUserName() {
        return userNmae;
    }

    public void setUserNmae(String userNmae) {
        this.userNmae = userNmae;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User : " + "user name = " + userNmae + ", password = " + password;
    }
}
